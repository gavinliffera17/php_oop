<?php
require('hewan.php');
require('ape.php');
require('frog.php');
$hewan = new Hewan("shaun");
echo "Nama Hewan = " .  $hewan->nama . "<br>";
echo "Jumlah Kaki = " . $hewan->legs . "<br>";
echo "Cold Blood = " . $hewan->cold_blood . "<br><br>";

$ape = new ape("kera sakti");
echo "Nama Hewan = " .  $ape->nama . "<br>";
echo "Jumlah Kaki = " . $ape->legs . "<br>";
echo "Cold Blood = " . $ape->cold_blood . "<br>";
echo $ape->Yell() . "<br><br>"; // "Auooo"

$frog = new frog("buduk");
echo "Nama Hewan = " .  $frog->nama . "<br>";
echo "Jumlah Kaki = " . $frog->legs . "<br>";
echo "Cold Blood = " . $frog->cold_blood . "<br>";
echo  $frog->Jump(); // "hop hop"
